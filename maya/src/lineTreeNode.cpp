/***************************************************************************
ptTreeNode.cpp  -  description
-------------------
    begin                : Mon Apr 3 2006
    copyright            : (C) 2006 by Julian Mann
    email                : julian.mann@gmail.com

    A node to store an array of points in a kdTree and make the tree data available in the dependency graph
	***************************************************************************/

#include <maya/MFnTypedAttribute.h>
#include <maya/MFnPluginData.h>

#include "errorMacros.h"
#include "lineTreeData.h"
#include "lineTreeNode.h"
#include "jMayaIds.h"


MTypeId     lineTreeNode::id( k_lineTreeNode );

//  stuff
//////////////////////////////////////////
MObject     lineTreeNode::aInput;
MObject 	lineTreeNode::aOutput;


lineTreeNode::lineTreeNode() {
	m_pd = new lineTreeData;
}

lineTreeNode::~lineTreeNode() {
	delete m_pd;
	m_pd = 0;
}

void *lineTreeNode::creator()
{
	return new lineTreeNode();
}

MStatus lineTreeNode::initialize()

//
{
	MStatus st;

	MFnTypedAttribute tAttr;

	// GEOMETRY
	aInput = tAttr.create("input", "in", MFnData::kDynSweptGeometry);
	tAttr.setReadable(false);
	tAttr.setArray(true);
	tAttr.setIndexMatters(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kDelete);
	tAttr.setCached(false);
	st = addAttribute( aInput ); er


	aOutput = tAttr.create("output", "out", lineTreeData::id);
	tAttr.setReadable(true);
	tAttr.setStorable(false);
	tAttr.setCached(false);
	addAttribute(aOutput);

	st = attributeAffects(aInput, aOutput ); er;

	return MS::kSuccess;
}


MStatus lineTreeNode::compute( const MPlug &plug, MDataBlock &data )

{
	if ( plug != aOutput) { return ( MS::kUnknownParameter ); }

	MStatus st;

	MArrayDataHandle hIn = data.inputArrayValue( aInput );

	m_pd->create(hIn);

	////////////////////////////////////////////////////////////////

	MDataHandle hOutput = data.outputValue(aOutput);
	MFnPluginData fnOut;
	MTypeId kdid(lineTreeData::id);

	MObject dOut = fnOut.create(kdid , &st ); er;
	lineTreeData *outTree = (lineTreeData *)fnOut.data(&st); er;
	// cerr << "data is copied here in preparation for the output " << endl;
	if (m_pd) {
		*outTree = (*m_pd);
	}
	//// cerr << "just setting the output data now" << endl;
	MDataHandle outputHandle = data.outputValue(lineTreeNode::aOutput, &st ); er;
	st = outputHandle.set(outTree); er;
	data.setClean( plug );

	return MS::kSuccess;
}



