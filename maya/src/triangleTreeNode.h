/***************************************************************************
                          triangleTreeNode.h  -  description
                             -------------------
    begin                : Fri Mar 31 2006
    copyright            : (C) 2006 by Julian Mann
    email                : julian.mann@gmail.com
 ***************************************************************************/
#ifndef _triangleTreeNode
#define _triangleTreeNode

#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include "triTreeData.h"
/// hello
class triangleTreeNode : public MPxNode
{
public:

						triangleTreeNode();

	virtual				~triangleTreeNode();

	virtual MStatus		compute( const MPlug& plug, MDataBlock& data );

	static  void*		creator();

	static  MStatus		initialize();

	static MObject aInMesh;

	static MObject aCurrentTime;

	static MObject aStartTime;
 
	static  MObject		aOutput;
	
	static MTypeId	id;

private:

	triTreeData * m_pd;
	MTime m_lastTimeIEvaluated;
	MPointArray m_lastPoints;
	bool m_lastFrameStatic;
};

#endif
