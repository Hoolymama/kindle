/***************************************************************************
                          triTreeNode.h  -  description
                             -------------------
    begin                : Fri Mar 31 2006
    copyright            : (C) 2006 by Julian Mann
    email                : julian.mann@gmail.com
 ***************************************************************************/
#ifndef _triTreeNode
#define _triTreeNode

#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include "triTreeData.h"

class triTreeNode : public MPxNode
{
public:

						triTreeNode();

	virtual				~triTreeNode();

	virtual MStatus		compute( const MPlug& plug, MDataBlock& data );

	static  void*		creator();

	static  MStatus		initialize();

	static  MObject		aInput;


	

	static  MObject		aOutput;
	
	static MTypeId	id;


private:

	triTreeData * m_pd;
};

#endif
