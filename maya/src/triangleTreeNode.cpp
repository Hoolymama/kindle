/***************************************************************************
                          ptTreeNode.cpp  -  description
                             -------------------
    begin                : Mon Apr 3 2006
    copyright            : (C) 2006 by Julian Mann
    email                : julian.mann@gmail.com

    A node to store an array of points in a kdTree and make the tree data available in the dependency graph
 ***************************************************************************/

#include <maya/MFnTypedAttribute.h>
#include <maya/MFnNumericAttribute.h>

#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnMesh.h>

#include <maya/MFnPluginData.h>
#include <maya/MAnimControl.h>
#include "errorMacros.h"
#include "triTreeData.h"
#include "triangleTreeNode.h"

#include "jMayaIds.h"

//
MTypeId     triangleTreeNode::id( k_triangleTreeNode );


//////////////////////////////////////////
MObject     triangleTreeNode::aInMesh;

MObject     triangleTreeNode::aCurrentTime;
MObject     triangleTreeNode::aStartTime;
 

MObject 	triangleTreeNode::aOutput;


triangleTreeNode::triangleTreeNode():m_lastPoints() {
	m_pd = new triTreeData;
	m_lastTimeIEvaluated = MAnimControl::currentTime();
	m_lastFrameStatic = false;
}

triangleTreeNode::~triangleTreeNode() {
	delete m_pd;
	m_pd = 0;
}

void* triangleTreeNode::creator()
{
	return new triangleTreeNode();
}

MStatus triangleTreeNode::initialize()

{
	MStatus st;
	MFnUnitAttribute uAttr;
	MFnTypedAttribute tAttr;
	
	aInMesh = tAttr.create("inMesh","im", MFnData::kMesh);
	tAttr.setReadable(false);
	st = addAttribute( aInMesh );er;

	aStartTime = uAttr.create( "startTime", "st", MFnUnitAttribute::kTime );
	uAttr.setStorable(true);
	uAttr.setKeyable(true);
	st =  addAttribute(aStartTime);  er;
	
	aCurrentTime = uAttr.create( "currentTime", "ct", MFnUnitAttribute::kTime );
	uAttr.setStorable(true);
	uAttr.setKeyable(true);
	st =  addAttribute(aCurrentTime);  er;

	aOutput = tAttr.create("output", "out", triTreeData::id);
	tAttr.setReadable(true);
	tAttr.setStorable(false);
	tAttr.setCached(false);
	addAttribute(aOutput);
	
 	st = attributeAffects(aInMesh, aOutput ); er;
 	st = attributeAffects(aCurrentTime, aOutput ); er;
 	st = attributeAffects(aStartTime, aOutput ); er;

	return MS::kSuccess;
}



MStatus triangleTreeNode::compute( const MPlug& plug, MDataBlock& data )

{
	if( plug != aOutput) return( MS::kUnknownParameter );

	// MString name = MFnDependencyNode(thisMObject()).name();

 	MStatus st;

  	MTime cT ,dT, sT;
    cT = data.inputValue(aCurrentTime).asTime();
    sT = data.inputValue(aStartTime).asTime();
    dT = cT - m_lastTimeIEvaluated;
    m_lastTimeIEvaluated = cT;
    double dt = dT.as( MTime::kSeconds );
    bool doReset = false;
    // cerr << "-------------------------------------------" << endl;
    // cerr << "sT: " << sT << endl;
    // cerr << "cT: " << cT << endl;
    if (cT <= sT || dt <= 0.0) {
    	doReset = true;
    }
    // cerr << "doReset: " << doReset << endl;

    // get the points
	MObject inMesh =  data.inputValue(aInMesh).asMesh();
	MFnMesh fnM(inMesh,&st );
	if (st.error()) return MS::kUnknownParameter ;
	MPointArray points;
	st = fnM.getPoints (points,  MSpace::kWorld );
	if (st.error()) return MS::kUnknownParameter ;

	MIntArray triangleCounts;
	MIntArray triangleVertices;
	st = fnM.getTriangles(triangleCounts,triangleVertices);
	unsigned pl = points.length();
	if (pl == 0) return MS::kUnknownParameter ;

	bool thisFrameStatic = true;

	if (pl != m_lastPoints.length() || doReset) {

		// if (pl != m_lastPoints.length() ) {
		// 	cerr << "pl != m_lastPoints.length(): " << pl << " != " <<  m_lastPoints.length() << endl;
		// }
		// cerr << "Resetting " << endl;
		// cerr << "doReset: " << doReset << endl;
		// cerr << "pl: " << pl << " - m_lastPoints.length()" << m_lastPoints.length() << endl;
		// cerr << "copy points to last points" << endl;
		m_lastPoints.copy(points);
		// thisFrameStatic = true;
	} else {
		// cerr << "NOT Resetting " << endl;
		// compare points against last points to determine if we are moving 
		// cerr << " compare points against last points " << endl;
		for (int i = 0; i < pl; ++i)
		{
			if (! points[i].isEquivalent(m_lastPoints[i])) {
				thisFrameStatic = false;
				break;
			}
		}
	}

	// cerr << "thisFrameStatic: " << thisFrameStatic << endl;
	// cerr << "m_lastFrameStatic: " << m_lastFrameStatic << endl;



	if ((thisFrameStatic && m_lastFrameStatic)) {
		// cerr << "Skipping" << endl;
		; // do nothing
		// cerr << "doing nothing" << endl;
	} else {
		// cerr << "computing triangleTreeNode" << endl;
		// cerr << "create tree" << endl;
		m_pd->create(m_lastPoints, points, triangleVertices);
	}
	m_lastFrameStatic = thisFrameStatic;
	m_lastPoints.copy(points);
	// cerr << "----------------------------------------" << endl;

	MDataHandle hOutput = data.outputValue(aOutput);
    MFnPluginData fnOut;
    MTypeId kdid(triTreeData::id);

  	MObject dOut = fnOut.create(kdid , &st );er;
  	triTreeData* outTree = (triTreeData*)fnOut.data(&st);er;
 //  cerr << "data is copied here in preparation for the output " << endl;
   if (m_pd) {
  		*outTree=(*m_pd);
   } 
  	//// cerr << "just setting the output data now" << endl;
  	MDataHandle outputHandle = data.outputValue(triangleTreeNode::aOutput, &st ); er;
  	st = outputHandle.set(outTree); er;
  	data.setClean( plug );
	return MS::kSuccess;
}



