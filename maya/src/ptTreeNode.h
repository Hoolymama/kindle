/***************************************************************************
                          ptTreeNode.h  -  description
                             -------------------
    begin                : Mon Apr 3 2006
    copyright            : (C) 2006 by Julian Mann
    email                : jpm@feta
 ***************************************************************************/

#ifndef _ptTreeNode
#define _ptTreeNode

#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include "ptTreeData.h"
/// hello
class ptTreeNode : public MPxNode
{
public:

						ptTreeNode();
///
	virtual				~ptTreeNode();
///
	virtual MStatus		compute( const MPlug& plug, MDataBlock& data );

	static  void*		creator();
///
	static  MStatus		initialize();
///
	static  MObject		aPoints;
	static  MObject		aVelocities;
	static  MObject		aNormals;
///
	// static MObject aSplitFunction;

	static  MObject		aOutput;

	static MTypeId	id;
///

private:
///
	
	enum SplitFunction { kLongestDimension, kCycleAxes };

	ptTreeData * m_pd;

};

#endif
