/***************************************************************************
lineTreeNode.h  -  description
-------------------
    begin                : Fri Mar 31 2006
    copyright            : (C) 2006 by Julian Mann
    email                : julian.mann@gmail.com
	***************************************************************************/
#ifndef _lineTreeNode
#define _lineTreeNode

#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include "lineTreeData.h"
/// hello
class lineTreeNode : public MPxNode
{
public:
	
	lineTreeNode();
	///
	virtual				~lineTreeNode();
	///
	virtual MStatus		compute( const MPlug& plug, MDataBlock& data );
	
	static  void*		creator();
	///
	static  MStatus		initialize();
	///
	static  MObject		aInput;
	///
	static  MObject		aOutput;
	
	static MTypeId	id;
	///
	
private:
		///
		lineTreeData * m_pd;
	
};

#endif
