/***************************************************************************
                          ptTreeNode.cpp  -  description
                             -------------------
    begin                : Mon Apr 3 2006
    copyright            : (C) 2006 by Julian Mann
    email                : julian.mann@gmail.com

    A node to store an array of points in a kdTree and make the tree data available in the dependency graph
 ***************************************************************************/

#include <maya/MFnTypedAttribute.h>
#include <maya/MFnNumericAttribute.h>

#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnUnitAttribute.h>

#include <maya/MFnPluginData.h>

#include "errorMacros.h"
#include "triTreeData.h"
#include "triTreeNode.h"

#include "jMayaIds.h"

//
MTypeId     triTreeNode::id( k_triTreeNode );

// Pool stuff
//////////////////////////////////////////
MObject     triTreeNode::aInput;

 
// MObject triTreeNode::aBBMinX;
// MObject triTreeNode::aBBMinY;
// MObject triTreeNode::aBBMinZ;
// MObject triTreeNode::aBBMin;
// MObject triTreeNode::aBBMaxX;
// MObject triTreeNode::aBBMaxY;
// MObject triTreeNode::aBBMaxZ;
// MObject triTreeNode::aBBMax;
// MObject triTreeNode::aBBSizX;
// MObject triTreeNode::aBBSizY;
// MObject triTreeNode::aBBSizZ;
// MObject triTreeNode::aBBSiz;
// MObject triTreeNode::aBB;
// MObject triTreeNode::aUseBB;
// MObject triTreeNode::aFrameCaching;
// MObject triTreeNode::aClampCache;
// MObject triTreeNode::aCacheStartTime;
// MObject triTreeNode::aCacheEndTime;


MObject 	triTreeNode::aOutput;


triTreeNode::triTreeNode() {
	m_pd = new triTreeData;
	// m_lastTimeIEvaluated = MAnimControl::currentTime();
}

triTreeNode::~triTreeNode() {
	delete m_pd;
	m_pd = 0;
}

void* triTreeNode::creator()
{
	return new triTreeNode();
}

MStatus triTreeNode::initialize()

{
	MStatus st;
	MFnUnitAttribute uAttr;
	MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnCompoundAttribute cAttr;
	// GEOMETRY
	aInput = tAttr.create("input", "in", MFnData::kDynSweptGeometry);
	tAttr.setReadable(false);
	tAttr.setArray(true);
	tAttr.setIndexMatters(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kDelete);
	tAttr.setCached(false);
	st = addAttribute( aInput ); er



	// aBBMinX = nAttr.create( "boxMinX", "bnx", MFnNumericData::kDouble);
	// aBBMinY = nAttr.create( "boxMinY", "bny", MFnNumericData::kDouble);
	// aBBMinZ = nAttr.create( "boxMinZ", "bnz", MFnNumericData::kDouble);
	// aBBMin = nAttr.create( "boxMin", "bn", aBBMinX, aBBMinY, aBBMinZ );
	// nAttr.setHidden(false);
	// st = addAttribute(aBBMinX);
	// st = addAttribute(aBBMinY);
	// st = addAttribute(aBBMinZ);
	// st = addAttribute(aBBMin);
	// aBBMaxX = nAttr.create( "boxMaxX", "bxx", MFnNumericData::kDouble);
	// aBBMaxY = nAttr.create( "boxMaxY", "bxy", MFnNumericData::kDouble);
	// aBBMaxZ = nAttr.create( "boxMaxZ", "bxz", MFnNumericData::kDouble);
	// aBBMax = nAttr.create( "boxMax", "bx", aBBMaxX,  aBBMaxY, aBBMaxZ );
	// nAttr.setHidden(false);
	// st = addAttribute(aBBMaxX);
	// st = addAttribute(aBBMaxY);
	// st = addAttribute(aBBMaxZ);
	// st = addAttribute(aBBMax);
	// aBBSizX = nAttr.create( "boxSizX", "bsx", MFnNumericData::kDouble);
	// aBBSizY = nAttr.create( "boxSizY", "bsy", MFnNumericData::kDouble);
	// aBBSizZ = nAttr.create( "boxSizZ", "bsz", MFnNumericData::kDouble);
	// aBBSiz = nAttr.create( "boxSiz", "bs", aBBSizX,  aBBSizY, aBBSizZ );
	// nAttr.setHidden(false);
	// st = addAttribute(aBBSizX);
	// st = addAttribute(aBBSizY);
	// st = addAttribute(aBBSizZ);
	// st = addAttribute(aBBSiz);
	// aBB = cAttr.create("inBoundingBox", "ibb");
	// cAttr.addChild(aBBMin);
	// cAttr.addChild(aBBMax);
	// cAttr.addChild(aBBSiz);
	// cAttr.setStorable(true);
	
	// cAttr.setHidden(false);
	
	// addAttribute(aBB);
	
	// aUseBB =  nAttr.create( "useBoundingBox", "ubb", MFnNumericData::kBoolean); 
	// nAttr.setKeyable(true);
	// nAttr.setStorable(true);
	// nAttr.setDefault(false);
	// st =	addAttribute(aUseBB);	er;
	
	// aFrameCaching =  nAttr.create( "frameCaching", "fch", MFnNumericData::kBoolean); 
	// nAttr.setKeyable(true);
	// nAttr.setStorable(true);
	// nAttr.setDefault(false);
	// st =	addAttribute(aFrameCaching);	er;
	
	// aClampCache =  nAttr.create( "clampCache", "clc", MFnNumericData::kBoolean); 
	// nAttr.setKeyable(true);
	// nAttr.setStorable(true);
	// nAttr.setDefault(false);
	// st =	addAttribute(aClampCache);	er;


	// aCacheStartTime = uAttr.create( "cacheStartTime", "cst", MFnUnitAttribute::kTime );
	// uAttr.setStorable(true);
	// uAttr.setKeyable(true);
	// st =  addAttribute(aCacheStartTime);  er;
	
	// aCacheEndTime = uAttr.create( "cacheEndTime", "cet", MFnUnitAttribute::kTime );
	// uAttr.setStorable(true);
	// uAttr.setKeyable(true);
	// st =  addAttribute(aCacheEndTime);  er;
	
	// aCurrentTime = uAttr.create( "currentTime", "ct", MFnUnitAttribute::kTime );
	// uAttr.setStorable(true);
	// uAttr.setKeyable(true);
	// st =  addAttribute(aCurrentTime);  er;

	aOutput = tAttr.create("output", "out", triTreeData::id);
	tAttr.setReadable(true);
	tAttr.setStorable(false);
	tAttr.setCached(false);

	addAttribute(aOutput);
	
//    st = attributeAffects(aBBMinX, aOutput ); er;
//    st = attributeAffects(aBBMinY, aOutput ); er;
//    st = attributeAffects(aBBMinZ, aOutput ); er;
//    st = attributeAffects(aBBMin, aOutput ); er;
//    
//    st = attributeAffects(aBBMaxX, aOutput ); er;
//    st = attributeAffects(aBBMaxY, aOutput ); er;
//    st = attributeAffects(aBBMaxZ, aOutput ); er;
//    st = attributeAffects(aBBMax, aOutput ); er;
	
 	st = attributeAffects(aInput, aOutput ); er;

//	st = attributeAffects(aFrameCaching, aOutput ); er;
//	st = attributeAffects(aCacheStartTime, aOutput ); er;
//	st = attributeAffects(aCacheEndTime, aOutput ); er;
	// st = attributeAffects(aCurrentTime, aOutput ); er;
//st = attributeAffects(aClampCache, aOutput ); er;
	
	

	return MS::kSuccess;
}


/*
we want to evaluate the geoConnector input only if we need its data
That is, if this frame needs output and no cache exists


MStatus triTreeNode::setDependentsDirty( const MPlug &plug, MPlugArray &affectedPlugs ) {
	MStatus	st;
	MString method("triTreeNode::setDependentsDirty");
	MTime csT =  data.outputValue( aCacheStartTime).asTime();
	MTime ceT =  data.outputValue( aCacheEndTime).asTime();
	MTime cT =   data.outputValue( aCurrentTime).asTime();
	
	cerr << method << endl;
	cerr << "plug being dirtied is " << plug.name() << endl;
	MObject thisNode = thisMObject();
	MFnDependencyNode fnThisNode( thisNode );
	// if this plug is a child of a compound who's first child's name starts with rman,
	// then we assume it is a render variable
	if (plug.isChild()) {
		MPlug firstChild = plug.parent().child(0);
		MString name(firstChild.partialName());
		cerr << "plug: "<< name << " plug(1,4): "<< name.substring(1,4) << endl;
		if (name.substring(1,4) == "rman") {
			 MPlug om = fnThisNode.findPlug( aOutMesh );
			 affectedPlugs.append( om );
		}
	}
	return( MS::kSuccess );
}
*/



MStatus triTreeNode::compute( const MPlug& plug, MDataBlock& data )

{
	if( plug != aOutput) return( MS::kUnknownParameter );

	
 	MStatus st;


	MString name = MFnDependencyNode(thisMObject()).name();


	MArrayDataHandle hIn = data.inputArrayValue( aInput );


	//cerr << "doCaching " <<  doCaching << endl;
    //	cerr << "doClampCache " <<  doClampCache << endl;
	
	// MTime csT = MTime(0.0);
	// MTime ceT = MTime(-1.0);
	// MTime cT =  MTime(0.0);
	
	// if (doCaching) {
	// 	cT =  data.inputValue( aCurrentTime).asTime();
	// 	csT =  data.inputValue( aCacheStartTime).asTime();
	// 	ceT =  data.inputValue( aCacheEndTime).asTime();
	
	// 	if (doClampCache) {
	// 		if (cT < csT) cT = csT;
	// 		if (cT > ceT) cT = ceT;
	// 	}
	// }
	
	// if (useBB) {
	// 	m_pd->create(hIn,box, cT,csT,ceT );
	// } else {
	// 	m_pd->create(hIn,MObject::kNullObj,MObject::kNullObj, cT,csT,ceT);
	// }
	// cerr << "building triTree" << endl;
	m_pd->create(hIn);
	// if (useBB) {
	// 	m_pd->create(hIn,box);
	// } else {
	// 	m_pd->create(hIn,MObject::kNullObj,MObject::kNullObj);
	// }

	////////////////////////////////////////////////////////////////

	MDataHandle hOutput = data.outputValue(aOutput);
    MFnPluginData fnOut;
    MTypeId kdid(triTreeData::id);

  	MObject dOut = fnOut.create(kdid , &st );er;
  	triTreeData* outTree = (triTreeData*)fnOut.data(&st);er;
 //  cerr << "data is copied here in preparation for the output " << endl;
   if (m_pd) {
  		*outTree=(*m_pd);
   } 
  	//// cerr << "just setting the output data now" << endl;
  	MDataHandle outputHandle = data.outputValue(triTreeNode::aOutput, &st ); er;
  	st = outputHandle.set(outTree); er;
  	data.setClean( plug );
//	cerr << " ************************out triTree " << name  << endl;
	return MS::kSuccess;
}



