/***************************************************************************
ptTreeNode.cpp  -  description
-------------------
    begin                : Mon Apr 3 2006
    copyright            : (C) 2006 by Julian Mann
    email                : julian.mann@gmail.com

    A node to store an array of points in a kdTree and make the tree data available in the dependency graph
	***************************************************************************/


#include <maya/MFnTypedAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnPluginData.h>
#include "errorMacros.h"
#include "ptTreeData.h"
#include "ptTreeNode.h"
#include "jMayaIds.h"

MTypeId     ptTreeNode::id( k_ptTreeNode );

// Pool stuff
//////////////////////////////////////////
MObject 	   ptTreeNode::aPoints;
MObject 	   ptTreeNode::aVelocities;
MObject 	   ptTreeNode::aNormals;
// MObject 	   ptTreeNode::aSplitFunction;

MObject 	   ptTreeNode::aOutput;


ptTreeNode::ptTreeNode() {
	m_pd = new ptTreeData;
}

ptTreeNode::~ptTreeNode() {
	delete m_pd;
	m_pd = 0;
}

void *ptTreeNode::creator()
{
	return new ptTreeNode();
}

MStatus ptTreeNode::initialize()

{
	MStatus st;

	MFnTypedAttribute tAttr;
	MFnEnumAttribute eAttr;


	aPoints = tAttr.create("points", "pts", MFnData::kVectorArray, &st); er;
	tAttr.setReadable(false);
	st = addAttribute(aPoints ); er;

	aVelocities = tAttr.create("velocities", "vls", MFnData::kVectorArray, &st); er;
	tAttr.setReadable(false);
	st = addAttribute(aVelocities ); er;

	aNormals = tAttr.create("normals", "nms", MFnData::kVectorArray, &st); er;
	tAttr.setReadable(false);
	st = addAttribute(aNormals ); er;

	// aSplitFunction = eAttr.create("splitFunction", "spf");
	// eAttr.addField("LongestDimension", kLongestDimension);
	// eAttr.addField("CycleAxes", kCycleAxes);
	// eAttr.setDefault(kLongestDimension);
	// eAttr.setKeyable(true);
	// eAttr.setWritable(true);
	// st = addAttribute(aSplitFunction );er;


	aOutput = tAttr.create("output", "out", ptTreeData::id);
	tAttr.setReadable(true);
	tAttr.setCached(false);

	addAttribute(aOutput);

	st = attributeAffects(aPoints, aOutput ); er;
	st = attributeAffects(aVelocities, aOutput ); er;
	st = attributeAffects(aNormals, aOutput ); er;

	return MS::kSuccess;
}


MStatus ptTreeNode::compute( const MPlug &plug, MDataBlock &data )

{
	if ( plug != aOutput) { return ( MS::kUnknownParameter ); }

	MStatus st;
	MDataHandle hP = data.inputValue( aPoints );
	MDataHandle hV = data.inputValue( aVelocities );
	MDataHandle hN = data.inputValue( aNormals );

	// int splitFunc = data.inputValue(aSplitFunction).asShort();


	// m_pd->create(hP,hV,hN,splitFunc);
	m_pd->create(hP, hV, hN);


	MDataHandle hOutput = data.outputValue(aOutput);
	MFnPluginData fnOut;
	MTypeId kdid(ptTreeData::id);
	MObject dOut = fnOut.create(kdid , &st ); er;
	ptTreeData *outTree = (ptTreeData *)fnOut.data(&st); er;

	if (m_pd) {
		*outTree = (*m_pd);
	}

	MDataHandle outputHandle = data.outputValue(ptTreeNode::aOutput, &st ); er;
	st = outputHandle.set(outTree); er;
	data.setClean( plug );

	return MS::kSuccess;
}

