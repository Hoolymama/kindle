/*
 *  plugin.cpp
 *  jtools
 *
 *  Created by Julian Mann on 25/11/2006.
 *  Copyright 2006 hooly|mama. All rights reserved.
 *
 */

#include <maya/MStatus.h>
#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>
#include "errorMacros.h"

#include "triTreeData.h"
#include "triTreeNode.h"
#include "lineTreeData.h"
#include "lineTreeNode.h"
#include "ptTreeData.h"
#include "ptTreeNode.h"
#include "triangleTreeNode.h"


 MStatus initializePlugin( MObject obj )
 {

 	MStatus st;
 	MString method("initializePlugin");

 	MFnPlugin plugin( obj, PLUGIN_VENDOR, PLUGIN_VERSION , MAYA_VERSION);


 	st = plugin.registerData( "lineTreeData", lineTreeData::id, lineTreeData::creator );er;

 	st = plugin.registerNode( "lineTreeNode", lineTreeNode::id, lineTreeNode::creator, lineTreeNode::initialize );er;

 	st = plugin.registerData( "triTreeData", triTreeData::id, triTreeData::creator );er;

 	st = plugin.registerNode( "triTreeNode", triTreeNode::id, triTreeNode::creator, triTreeNode::initialize );er;

 	st = plugin.registerNode( "triangleTreeNode", triangleTreeNode::id, triangleTreeNode::creator, triangleTreeNode::initialize );er;

 	st = plugin.registerData( "ptTreeData", ptTreeData::id, ptTreeData::creator );er;

 	st = plugin.registerNode( "ptTreeNode", ptTreeNode::id, ptTreeNode::creator, ptTreeNode::initialize );er;

	MGlobal::executePythonCommand("import kindle;kindle.load()");

 	return st;

 }

 MStatus uninitializePlugin( MObject obj)
 {
 	MStatus st;

 	MString method("uninitializePlugin");

 	MFnPlugin plugin( obj );

 	st = plugin.deregisterNode( ptTreeNode::id );er;

 	st = plugin.deregisterData( ptTreeData::id );er;

 	st = plugin.deregisterNode( triangleTreeNode::id );er;

 	st = plugin.deregisterNode( triTreeNode::id );er;

 	st = plugin.deregisterData( triTreeData::id );er;

 	st = plugin.deregisterNode( lineTreeNode::id );er;

 	st = plugin.deregisterData( lineTreeData::id );er;


 	return st;
 }


